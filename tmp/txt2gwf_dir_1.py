import argparse
import gwpy, glob
from gwpy import timeseries
import os
parser = argparse.ArgumentParser(description='Convert txt files to gwf files')
parser.add_argument('-data_dir','--data_dir', help='Data directory',required=True)
parser.add_argument('-out_dir','--out_dir',help='OUTPUT GWF directory')
args = parser.parse_args()
try:
    os.makedirs(args.out_dir)
except:
    print('dir exists')
data_dir = args.data_dir
files= glob.glob(data_dir+'/event_prec_*.txt')
channel_dict={'H1':'H1:DCS-CALIB_STRAIN_CLEAN_C01', 'L1':'L1:DCS-CALIB_STRAIN_CLEAN_C01', 'V1':'V1:Hrec_hoft_16384Hz'}

for f in files:
    data = timeseries.TimeSeries.read(f)
    det = f.split('_')[-2]
    f1 = f.split('/')[-1]
    data.channel=channel_dict[det]#'{}:DCS-CALIB_STRAIN_CLEAN_C01'.format(det)
    #data.write(args.out_dir+'/'+f1.split('.txt')[0]+'.gwf')
    data.write(args.out_dir + '/' + det + '.gwf')
    print('{} converted to {}....'.format(f,args.out_dir+ '/'+ det + '.gwf'))
