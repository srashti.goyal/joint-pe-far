import argparse
import gwpy, glob
from gwpy import timeseries

parser = argparse.ArgumentParser(description='Convert txt files to gwf files')
parser.add_argument('-data_dir','--data_dir', help='Data directory',required=True)
args = parser.parse_args()

data_dir = args.data_dir
files= glob.glob(data_dir+'/*.txt')
channel_dict={'H1':'H1:DCS-CALIB_STRAIN_CLEAN_C01', 'L1':'L1:DCS-CALIB_STRAIN_CLEAN_C01', 'V1':'V1:Hrec_hoft_16384Hz'}

for f in files:
        data = timeseries.TimeSeries.read(f)
        det = f.split('_')[2]
        data.channel=channel_dict[det]#'{}:DCS-CALIB_STRAIN_CLEAN_C01'.format(det)
        data.write(f.split('.txt')[0]+'.gwf')
        print('{} converted to {}....'.format(f,f.split('.txt')[0]+'.gwf'))
                                                                           