import numpy as np
import pandas as pd
import os
import json
import argparse

parser = argparse.ArgumentParser(description='generate ini files and outdirs for joint-pe-hanabi injecion')
parser.add_argument('-index','--index', help='injection pair no.',required=True)
parser.add_argument('-inj_file','--inj_file',help='input injection pairs',default='../haris_GW19O727/GW190727_LHV_LHV_Bext_above_minus5.dat')
parser.add_argument('-data_dir','--data_dir',help='injection gwfs directory',default='/home/srashti.goyal/joint-pe-far/haris_GW19O727/converted/')
parser.add_argument('-prior_file','--prior_file',help='prior file to use  S190424ao.prior/S190719an.prior',default='S190424ao.prior')
parser.add_argument('-out_dir','--out_dir',help='output directory ../run_dir/DIR' ,default= 'test')
parser.add_argument('-submit','--submit',help='submit condor_dags yes/no?',default='no')
parser.add_argument('-submit_only','--submit_only',help='only submit condor_dags yes/no?',default='no')

args = parser.parse_args()

inj_file=args.inj_file
trigger_pairs=np.genfromtxt(inj_file,skip_header=True)


data_dir = args.data_dir
bilby_dirs = '../run_dir/' + args.out_dir

index = int(args.index)

t1,t2 = trigger_pairs[index,:2].astype('int')
pair_dir=str(t1)+'_'+str(t2)
shell_comm= '%s/%s'%(bilby_dirs,pair_dir)
t1_ini='%s/%s/%s'%(bilby_dirs,pair_dir,str(t1)+'.ini')
t2_ini = '%s/%s/%s'%(bilby_dirs,pair_dir,str(t2)+'.ini')
joint_ini = '%s/%s/joint_%s'%(bilby_dirs,pair_dir,pair_dir+'.ini')

if args.submit_only =='no':
    print('generating ini files for triggers %d and %d'%(t1,t2))

    with open(data_dir+ str(t1)+'/'+'inj_params_noiseydata.json') as f:
         t_gps_t1= json.load(f)['t_gps']
    with open(data_dir+ str(t2)+'/'+'inj_params_noiseydata.json') as f:
         t_gps_t2= json.load(f)['t_gps']

    try:
        os.makedirs(bilby_dirs+'/'+pair_dir)
    except:
        print('dir exists, deleting outdirs')
        os.system('rm -r %s/outdir_*'%(bilby_dirs+'/'+pair_dir))
    os.system('scp trigger.ini %s'%t1_ini)
    os.system('scp trigger.ini %s'%t2_ini)
    os.system('scp joint_trigger.ini %s'%joint_ini)


    text_file = open(t1_ini, "a")
    text_file.write('prior-file = ../../../scripts/%s \n' %args.prior_file)
    text_file.write('data-dict={H1:' + data_dir+ str(t1)+'/H1.gwf,L1:'+ data_dir+ str(t1)+'/L1.gwf, V1:'+ data_dir+ str(t1)+'/V1.gwf}\n')
    text_file.write('label = %d \n' %t1)
    text_file.write('outdir = outdir_%d \n' %t1)
    text_file.write('trigger-time = %.1f \n' %t_gps_t1)
    
    text_file.close()



    text_file = open(t2_ini, "a")
    text_file.write('prior-file = ../../../scripts/%s \n' %args.prior_file)
    text_file.write('data-dict={H1:' + data_dir+ str(t2)+'/H1.gwf,L1:'+ data_dir+ str(t2)+'/L1.gwf, V1:'+ data_dir+ str(t2)+'/V1.gwf}\n')
    text_file.write('label = %d \n' %t2)
    text_file.write('outdir = outdir_%d \n' %t2)
    text_file.write('trigger-time = %.1f \n' %t_gps_t2)
    text_file.close()


    text_file = open(joint_ini, "a")
    text_file.write('label = %d_%d \n' %(t1,t2))
    text_file.write('outdir = outdir_%d_%d \n' %(t1,t2))
    text_file.write('trigger-ini-files = [%d.ini,%d.ini] \n' %(t1,t2))
    text_file.close()

    text_file = open(shell_comm + '/run.sh', "w")
    text_file.write('hanabi_joint_pipe joint_%s.ini\n' %pair_dir)
    text_file.close()


    os.system('cd %s \n sh run.sh'%shell_comm)

if args.submit == 'yes':
    print('we are submitting the individual triggers now')
    os.system('cd %s \n  condor_submit_dag outdir_%s/submit/dag_%s.submit'%(shell_comm,t1,t1))
    os.system('cd %s \n  condor_submit_dag outdir_%s/submit/dag_%s.submit'%(shell_comm,t2,t2))
    import time
    print('\n waiting 150s before submitting the joint dag\n')
    time.sleep(150)
    os.system('cd %s \n  condor_submit_dag outdir_%s/submit/dag_%s.submit'%(shell_comm,pair_dir,pair_dir))

if args.submit_only == 'yes':
    print('we are submitting the only joint triggers now')
    os.system('cd %s \n  condor_submit_dag outdir_%s/submit/dag_%s.submit'%(shell_comm,t1,t1))
    os.system('cd %s \n  condor_submit_dag outdir_%s/submit/dag_%s.submit'%(shell_comm,t2,t2))
   # import time
   # time.sleep(150)
  #  print('\n waiting 150s before submitting the joint dag\n')
    os.system('cd %s \n  condor_submit_dag outdir_%s/submit/dag_%s.submit'%(shell_comm,pair_dir,pair_dir))
