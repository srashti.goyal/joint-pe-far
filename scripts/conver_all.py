import gwpy, glob
from gwpy import timeseries
import os

## This script is used to convert all the .txt strain data in the data_loc directory(in CIT) to .gwf data in the out_dir ##

data_loc='/home/haris.k/runs/lensing/O3/o3a-lensing-injection-simulation/data/GW190915/LHV_LHV/Bext/data1/'

#GW190727: '/home/haris.k/runs/lensing/O3/o3a-lensing-injection-simulation/data/GW190727/LHV_LHV/Bext/data1/'
#GW19O915: '/home/haris.k/runs/lensing/O3/o3a-lensing-injection-simulation/data/GW190915/LHV_LHV/Bext/data1/'

data_dirs=os.listdir(data_loc)
out_dir = '../haris_GW19O915/converted/'

n = len(data_dirs)
channel_dict={'H1':'H1:DCS-CALIB_STRAIN_CLEAN_C01', 'L1':'L1:DCS-CALIB_STRAIN_CLEAN_C01', 'V1':'V1:Hrec_hoft_16384Hz'}
print('no. of injections = ', n)

for i in range(n):
    data_dir = data_dirs[i]
    # get the .txt file for each injection.
    files= glob.glob(data_loc+data_dir+'/event_prec_*.txt')
    try:
        os.makedirs(out_dir+data_dir)
    except:
        print('dir exists')
    # copy injection parameters json file.
    os.system('scp %s/inj_params_noiseydata.json %s/'%(data_loc+data_dir,out_dir+data_dir))

    # convert to .gwf and save
    for f in files:
        data = timeseries.TimeSeries.read(f)
        det = f.split('_')[-2]
        f1 = f.split('/')[-1]
        data.channel=channel_dict[det]
        fname=out_dir + data_dir+'/'+ det + '.gwf'

        data.write(fname)
        print('{} converted to {}....'.format(f,fname))
