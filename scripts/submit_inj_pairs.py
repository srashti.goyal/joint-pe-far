import os
import argparse
import glob
import numpy as np
from numpy.core.defchararray import add
import sys

parser = argparse.ArgumentParser(description='generate and submit ini files and outdirs for joint-pe-hanabi injection set')
parser.add_argument('-out_dir','--out_dir',help='output directory ../run_dir/DIR' ,default= 'test')
parser.add_argument('-submit','--submit',help='submit condor_dags yes/no?',default='no')
parser.add_argument('-idx0','--idx0',help='start from inj no.',default=0)
parser.add_argument('-n','--n',help='number of injections',default=10)
parser.add_argument('-submit_only','--submit_only',help='only submit condor_dags yes/no?',default='no')
#parser.add_argument('-inj_file','--inj_file',help='input injection pairs',default='../haris_GW19O727/GW190727_LHV_LHV_Bext_above_minus5.dat')
#parser.add_argument('-data_dir','--data_dir',help='injection gwfs directory',default='../haris_GW19O727/converted/')

parser.add_argument('-event','--event',help='injection study of event GW190727/GW190915',default='GW190727')
parser.add_argument('-resubmit_failed','--resubmit_failed',help='only resubmit_failed pairs yes/no?',default='no')
args = parser.parse_args()

if args.event == 'GW190727':
    inj_file = '../haris_GW190727/GW190727_LHV_LHV_Bext_above_minus5.dat'
    data_dir = '/home/srashti.goyal/joint-pe-far/haris_GW190727/converted/'
    prior_file = 'S190424ao.prior'
elif args.event == 'GW190915':
    inj_file = '../haris_GW190915/GW190915_LHV_LHV_Bext_above_minus5.dat'
    data_dir = '/home/srashti.goyal/joint-pe-far/haris_GW190915/converted/'
    prior_file = 'S190719an.prior'
else:
    print('event not recognised,choose either GW19O727 or GW19O915')
    sys.exit()

if args.resubmit_failed == 'no':
    for i in range(int(args.idx0),int(args.idx0)+int(args.n)):
        os.system('python create_inis.py --out_dir %s --index %d --submit %s --submit_only %s --inj_file %s --data_dir %s --prior_file %s'%(args.out_dir,i,args.submit,args.submit_only,inj_file,data_dir,prior_file))
else:
    failed_pairs = glob.glob('../run_dir/'+args.out_dir + '/**/outdir*/submit/*.rescue001')
    trigger_pairs=np.genfromtxt(args.inj_file,skip_header=True).astype('int')
    pair_dirs=add(add(trigger_pairs[:,0].astype('str') , '_') , trigger_pairs[:,1].astype('str'))
    for failed_pair in failed_pairs:
        t1_t2=(failed_pair.split('/out')[0]).split('/')[-1]
        index=np.where(pair_dirs==t1_t2)[0]
      #  print(index, t1_t2)
        os.system('python create_inis.py --out_dir %s --index %d --submit %s --submit_only %s --inj_file %s --data_dir %s --prior_file %s'%(args.out_dir,index,'yes','no',inj_file,data_dir,prior_file))
